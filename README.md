# wsdm-utils

Various utilities and helpers used by `wsdm-` tools.

[Documentation](https://lapidusinteractive.gitlab.io/wsdm-utils)

[Lapidus Interactive](http://lapidus.se)