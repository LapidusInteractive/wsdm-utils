import { niceNum } from "../"
import { strictEqual } from "assert"

describe("niceNum", () => {
  it("returns '…' when input is not numeric", () => {
    strictEqual(niceNum("abc"), "…")
  })
  it("doesn't format numbers smaller than 10000", () => {
    strictEqual(niceNum(1), "1")
    strictEqual(niceNum(9999), "9999")
  })
  it("adds 'k' suffix to numbers in a thousand range (< 1e6)", () => {
    strictEqual(niceNum(1e5), "100k")
  })
  it("adds 'm' suffix to numbers in a million range (< 1e9)", () => {
    strictEqual(niceNum(1e8), "100m")
  })
  it("adds 'bn' suffix to numbers in a billion range (< 1e12)", () => {
    strictEqual(niceNum(1e11), "100bn")
  })
  it("adds 'tr' suffix to numbers in a trillion range (>= 1e12)", () => {
    strictEqual(niceNum(1e12), "1tr")
    strictEqual(niceNum(1e13), "10tr")
  })
  it("rounds to specified decimals", () => {
    strictEqual(niceNum(112345, 2), "112.34k")
    strictEqual(niceNum(1.12345, 4), "1.1235")
    strictEqual(niceNum(1.12345, 3), "1.123")
    strictEqual(niceNum(1.12345, 2), "1.12")
    strictEqual(niceNum(1.12345, 1), "1.1")
    strictEqual(niceNum(1.12345, 0), "1")
  })
})
