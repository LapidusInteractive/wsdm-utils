import { isNumeric } from "../"
import { strictEqual } from "assert"

describe("isNumeric", () => {
  it("returns true when the argument is number", () => {
    strictEqual(isNumeric(1), true)
    strictEqual(isNumeric(1e6), true)
  })
  it("returns true when the argument is string that can be parsed as number", () => {
    strictEqual(isNumeric("1"), true)
    strictEqual(isNumeric("1.2"), true)
  })
  it("returns false when the argument is null", () => {
    strictEqual(isNumeric(null), false)
  })
  it("returns false when the argument is undefined", () => {
    strictEqual(isNumeric(undefined), false)
  })
  it("returns false when the argument is empty string", () => {
    strictEqual(isNumeric(""), false)
  })
  it("returns false when the argument is string that can't be parsed as number", () => {
    strictEqual(isNumeric("abc"), false)
  })
})
