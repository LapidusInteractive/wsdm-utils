import isNumeric from "./isNumeric.js"
import niceNum from "./niceNum.js"

export {
  isNumeric,
  niceNum
}
