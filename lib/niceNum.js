import { formatPrefix } from "d3-format"
import isNumeric from "./isNumeric"

/**
 * Formats the input value into a human friendly version, rounds up to the
 * precision argument
 *
 * @func
 * @sig (Number, Number) -> String
 * @param {Number} input The value to format.
 * @param {Number} precision Decimals length.
 * @return {String} Formatted string.
 * @example
 *
 * niceNum(1000); //=> "1000"
 * niceNum(100.542, 1); //=> "100.5"
 * niceNum(10000); //=> "10k"
 * niceNum("abc"); //=> "…"
 * niceNum("N/A"); //=> "N/A"
 */
const niceNum = (input, precision) => {
  if (!isNumeric(input)) return "…"
  if (input === "N/A") return input
  if (Math.abs(input) < 10000) return input.toFixed(precision)

  const prefixes = { k: "k", M: "m", G: "bn", T: "tr" }
  const formatted = formatPrefix("k", input)(input)
  const scaled = +formatted.slice(0, -1)
  const symbol = formatted.slice(-1)
  return `${scaled.toFixed(precision)}${prefixes[symbol]}`
}

export default niceNum
